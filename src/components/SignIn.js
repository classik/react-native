import React from 'react';

import {
  Text,
  View,
  TextInput,
  StyleSheet
} from 'react-native';

var Button = require('./common/Button');
var validator = require('email-validator');
var api = require('../utils/api');
var userStorage = require('../storage/user');
var Facebook = require('./common/Facebook');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'red'
  },
  input : {
    padding: 4,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    margin: 5,
    width: 200,
    alignSelf: 'center'
  },
  label: {
    fontSize: 18
  }
});

module.exports = React.createClass({
  getInitialState: function() {
    return {
      email: '',
      password: '',
      errorMessage: ''
    }
  },
  onPressSignIn: function() {
    // Checking status code
    // api.ping().then((status_code) => {
    //   console.log(status_code);
    // });
    var email = this.state.email,
        password = this.state.password;
    if (!validator.validate(email)) {
      this.setState({errorMessage: 'Email not valid'});
    } else if (password === '') {
      this.setState({errorMessage: 'Password is empty'});
    } else {
      this.setState({errorMessage: ''});
      api.signIn(email, password)
            .then((data) => {
              if (data.status_code === 200) {
                userStorage.write(data);
                api.getStudyList(data.response.data.token).then((response) => {
                    this.props.navigator.immediatelyResetRouteStack([{name: 'study_list', data: response, message: ''}]);
                });
              } else if (data.status_code === 400) {
                this.setState({errorMessage: data.response.message});
              }
            });
    }
  },
  onPressSignUp: function() {
    // Checking status code
    // PingApi().then((status_code) => {
    //   console.log(status_code);
    // });
    this.props.navigator.immediatelyResetRouteStack([{name: 'signup'}]);
  },
  render: function() {
    var showErr = (
      <Text style={styles.label}>{this.state.errorMessage}</Text>
    );
    return (
      <View style={styles.container}>
        <Text>Sign in</Text>
        <Text style={styles.label}>Email:</Text>
        <TextInput
          style={styles.input}
          value={this.state.email}
          onChangeText={(text) => this.setState({email: text})}
          autoCapitalize={'none'}
          placeholder={'You email'}
          />

        <Text style={styles.label}>Password:</Text>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          value={this.state.password}
          onChangeText={(text) => this.setState({password: text})}
          autoCapitalize={'none'}
          placeholder={'Password'}
          />

        {showErr}
        <Button text={'Sign in'} onPress={this.onPressSignIn} />
        <Button text={'Sign up'} onPress={this.onPressSignUp} />

        <Facebook route={this.props.route} navigator={this.props.navigator}/>
      </View>
    );
  }
});
