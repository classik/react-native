import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  AsyncStorage
} from 'react-native';

var UserStorage = require('../storage/user');

module.exports = React.createClass({
  render: function () {
    return (
      <View style={styles.container}>
        <Text>Cards</Text>
        {UserStorage.read()}
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'blue'
  }
});
