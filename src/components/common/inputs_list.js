import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  TextInput
} from 'react-native';

var UserStorage = require('../../storage/user');

module.exports = React.createClass({
  getInitialState: function() {
    return {
      title: '',
      countInputs: 2,
      inputs: [
        <View style={styles.input_wrap_words} key={0}>
          <TextInput
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Term'}
            />
          <TextInput
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Definision'}
            />
        </View>
      ]
    }
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.input_wrap}>
          <TextInput
            style={styles.input}
            value={this.state.title}
            onChangeText={(text) => this.setState({title: text})}
            autoCapitalize={'none'}
            placeholder={'Title'}
            />
        </View>
        {this.state.inputs}
      </View>
    );
  },
  renderTextInputs: function() {
    var inputs = [];
    for (var i = 0; i< this.props.countInputs; i++) {
      inputs.push(
        <View style={styles.input_wrap_words} key={i}>
          <TextInput
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Term'}
            />
          <TextInput
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Definision'}
            />
        </View>
      );
    }
    this.setState({inputs: inputs})
  }
});

var styles = StyleSheet.create({
  container : {
    alignItems: 'center',
    justifyContent: 'center',
  },
  input_wrap: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input_wrap_words: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input : {
    padding: 4,
    height: 30,
    margin: 2,
    alignSelf: 'center',
    flex: 1
  },
  label: {
    fontSize: 14
  }
});
