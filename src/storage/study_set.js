import React from 'react';

import {
  AsyncStorage
} from 'react-native';

var studySet = {
  studySetPrefix : 'study_set_',
  write(storageKey, data) {
      try {
        if (storageKey !== '') {
          key = storageKey;
          data.updated = Date.now().toString();
        } else {
          key = this.studySetPrefix + Date.now().toString();
          data.updated = '';
        }
        data.created = Date.now().toString();
        data.deleted = '';
        AsyncStorage.setItem(key, JSON.stringify(data));

        return key;
      } catch (error) {
        console.log('Error saving data');
      }
  },
  async getByKey(key) {
    try {
      const value = await AsyncStorage.getItem(key);
      return value;
    } catch(error) {
      console.log('Error getting study set data');
    }
  },
  async getAll() {
    try {
      const value = await AsyncStorage.getAllKeys();
      return value;
    } catch (e) {
      console.log('Error getting all keys');
    }
  },
}

module.exports = studySet;
