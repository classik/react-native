import React from 'react';

import {
  View,
  StyleSheet,
  Navigator
} from 'react-native';

var SignUp = require('./components/SignUp');
var Signin = require('./components/SignIn');
var Cards = require('./flashcards/cards');
var StudyList = require('./components/StudyList');
var StudyListCreate = require('./components/StudyListCreate');

var ROUTES = {
  signup: SignUp,
  cards: Cards,
  signin: Signin,
  study_list: StudyList,
  study_list_create: StudyListCreate
};

module.exports = React.createClass({
    renderScene: function (route, navigator) {
      var Component = ROUTES[route.name];
      return <Component route={route} navigator={navigator} data={route.data} message={route.message}/>
    },
    render: function () {
      return (
        <Navigator
        style={styles.container}
        initialRoute={{name: 'signin'}}
        renderScene={this.renderScene}
        configureScene={() => {return Navigator.SceneConfigs.FloatFromRight; }}
      />
      );
    }
});

var styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
