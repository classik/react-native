import React from 'react';

import {
  View,
  StyleSheet,
  TextInput
} from 'react-native';

var styles = StyleSheet.create({
  input_wrap_words: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input: {
    padding: 4,
    height: 30,
    margin: 2,
    alignSelf: 'center',
    flex: 1
  }
});

module.exports = React.createClass({
  getInitialState: function(){
    return {
      textDefinision: "",
      textTerm: ""
    }
  },
  changeTextDefinision: function(text) {
    this.setState({
      textDefinision: text,
    })
    this.props.changeInput({
      textDefinision: text,
      textTerm: this.state.textTerm
    });
  },
  changeTextTerm: function(text) {
    this.setState({
      textTerm: text,
    })
    this.props.changeInput({
      textDefinision: this.state.textDefinision,
      textTerm: text
    });
  },
  render: function(){

    return (
      <View>
        <View style={styles.input_wrap_words}>
          <TextInput
            onChangeText={this.changeTextTerm}
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Term'}
            value={this.state.textTerm}
            />
          <TextInput
            onChangeText={this.changeTextDefinision}
            value={this.state.textDefinision}
            style={styles.input}
            autoCapitalize={'none'}
            placeholder={'Definision'}
            />
        </View>
      </View>
    )
  }
})
