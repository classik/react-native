var api = {
  registerUser(email, first_name, last_name, isFacebook, link, picture, password) {
    var url = 'http://wordslist.dev/api/register-user';
    var body = `email=${email}&first_name=${first_name}&last_name=${last_name}&facebook=${isFacebook}&link=${link}&picture=${picture}&password=${password}`;
    return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: body
      })
      .then((response) => {
          return response.json();
      })
      .then((responseData) => {
          return responseData;
      })
      .catch((error) => {
          console.log(error);
      });
  },
  signIn(email, password) {
    var url = 'http://wordslist.dev/api/signin';
    var body = `email=${email}&password=${password}`;
    return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: body
      })
      .then((response) => {
          return response.json();
      })
      .then((responseData) => {
          return responseData;
      })
      .catch((error) => {
          console.log(error);
      });
  },
  ping() {
    var url = 'http://wordslist.dev/api/ping';
    return fetch(url, {
        method: "POST"
      })
      .then((response) => {
          return response.status;
      })
      .catch((error) => {
          console.log(error);
      });
  },
  getStudyList(token) {
    var url = 'http://wordslist.dev/api/get-study-set';
    var body = `token=${token}`;
    return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: body
      })
      .then((response) => {
          return response.json();
      })
      .then((responseData) => {
          return responseData;
      })
      .catch((error) => {
          console.log(error);
      });
  },
  saveStudySet(token, title, words, lang) {
    var url = 'http://wordslist.dev/api/save-study-set';
    var body = {
        token : token,
        title: title,
        words: words,
        lang: lang,
    };
    return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: JSON.stringify(body)
      })
      .then((response) => {
          return response.json();
      })
      .then((responseData) => {
          return responseData;
      })
      .catch((error) => {
          console.log(error);
      });
  },
}

module.exports = api;
