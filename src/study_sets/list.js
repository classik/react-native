import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  AsyncStorage
} from 'react-native';

var UserStorage = require('../storage/user');
var CreatSetButton = require('../components/common/button');

module.exports = React.createClass({
  render: function () {
    return (
      <View style={styles.container}>
        <View style={styles.create_btn}>
          <CreatSetButton text={'Create a new study set'} onPress={this.onPressCreateSet} />
        </View>
        <View style={styles.content}>
          <View>
            <Text>Date 1</Text>
          </View>
          <View>
            <Text>Set #1</Text>
          </View>
        </View>
      </View>
    );
  },
  onPressCreateSet: function() {
    this.props.navigator.immediatelyResetRouteStack([{name: 'create_study_set'}]);
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'blue'
  },
  content: {
    flex: 0.9,
    borderWidth: 1,
    borderColor: 'red'
  },
  create_btn: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: 'green',
    paddingTop: 20,
    paddingBottom: 10,
    alignItems: 'center'
  }
});
