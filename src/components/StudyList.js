import React from 'react';

import {
  Text,
  View,
  ListView,
  StyleSheet,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';

var Button = require('./common/Button');
var userStorage = require('../storage/user');
var studySetStorage = require('../storage/study_set');
var api = require('../utils/api');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'blue'
  },
  content: {
    flex: 0.9,
    borderWidth: 1,
    borderColor: 'red'
  },
  create_btn: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: 'green',
    paddingTop: 20,
    paddingBottom: 10,
    alignItems: 'center'
  },
  rowContainer: {
    padding: 10,
  }
});

module.exports = React.createClass({
  getInitialState: function() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    var data = [{}];
    if (this.props.data.length != 0) {
        data = this.props.data.response.data;
    }
    if (this.props.message != '') {
      alert(this.props.message);
    }
    return {
      dataSource: ds.cloneWithRows(data),
      studySetKeys: [],
      studySetValues: [],
    }
  },
  renderRow: function(rowData) {
        return (
          <TouchableHighlight
            underlayColor={'#dee1e5'}
            onPress={() => this.onPressStudyItem(rowData.id)}
            >
          <View>
            <View>
              <Text>{rowData.hasOwnProperty('created') ? rowData.created.sec : ''}</Text>
            </View>
            <View>
              <Text>{rowData.hasOwnProperty('title') ? rowData.title : ''}</Text>
            </View>
          </View>
          </TouchableHighlight>
        );
  },
  render: function () {
    return (
      <View style={styles.container}>
        <View style={styles.create_btn}>
          <Button text={'Create a new study set'} onPress={this.onPressCreateSet} />
        </View>
        <View style={styles.content}>
        <ListView
          enableEmptSections={true}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow} />
        </View>
      </View>
    );
  },
  onPressCreateSet: function() {
    this.props.navigator.immediatelyResetRouteStack([{name: 'study_list_create'}]);
  },
  onPressStudyItem: function (id) {
    console.log(id);
  }
});
