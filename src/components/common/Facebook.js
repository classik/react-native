import React from 'react';

import {
  View,
  Text,
  StyleSheet
} from 'react-native';

const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} = FBSDK;

var api = require('../../utils/api');
var userStorage = require('../../storage/user');

var styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: 'green',
    margin: 5,
    alignSelf: 'center'
  }
});

module.exports = React.createClass({
  render: function () {
    return (
      <View style={styles.container}>
        <LoginButton
          publishPermissions={["publish_actions"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log(error);
              } else if (result.isCancelled) {
                alert("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    let accessToken = data.accessToken;

                    const responseInfoCallback = (error, result) => {
                      if (error) {
                        console.log(error);
                      } else {
                        result.isFacebook = true;
                        api.registerUser(result.email, result.first_name, result.last_name, true, result.link, result.picture.data.url, '')
                          .then((data) => {
                            userStorage.write(data);
                            api.getStudyList(data.response.data.token).then((response) => {
                                this.props.navigator.immediatelyResetRouteStack([{name: 'study_list', data: response, message: ''}]);
                            });
                          });
                      }
                    }

                    const infoRequest = new GraphRequest(
                      '/me',
                      {
                        accessToken: accessToken,
                        parameters: {
                          fields: {
                            string: 'email,name,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified'
                          }
                        }
                      },
                      responseInfoCallback
                    );

                    new GraphRequestManager().addRequest(infoRequest).start();
                  }
                )
              }
            }
          }
          onLogoutFinished={() => alert("logout.")}/>
          </View>
    );
  }
});
