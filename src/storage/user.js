import React from 'react';

import {
  AsyncStorage
} from 'react-native';

var USER_KEY = 'user';

var user = {
  write(result) {
      try {
        var userObj = {
          user_id: result.response.data.user_id,
          token: result.response.data.token,
        };
        AsyncStorage.setItem(USER_KEY, JSON.stringify(userObj));
        console.log(JSON.stringify(userObj));
      } catch (error) {
        console.log('Error saving data');
      }
  },
  read() {
    try {
      AsyncStorage.getItem(USER_KEY).then((value) => {
        console.log(JSON.parse(value));
      }).done();
    } catch(error) {
      console.log('Error getting user data');
    }
  }
}

module.exports = user;
