import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  AsyncStorage,
  TouchableHighlight,
  ListView,
  TextInput,
} from 'react-native';

var UserStorage = require('../storage/user');
var CreatSetButton = require('../components/common/button');
var InputsList = require('../components/common/inputs_list');


var styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  content: {
    flex: 1,
  },
  toolbar:{
        backgroundColor:'#81c04d',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row'
  },
  toolbarButton:{
      width: 50,
      color:'#fff',
      textAlign:'center'
  },
  toolbarTitle:{
      color:'#fff',
      textAlign:'center',
      fontWeight:'bold',
      flex:1
  },
  plusButton: {
    color:'#fff',
    textAlign:'right',
    fontWeight:'bold',
    fontSize: 22
  },
  footer: {
    position: 'absolute',
    left:0,
    right:0,
    bottom:0,
    backgroundColor:'#81c04d',
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight:10
  },
  input_wrap: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input_wrap_words: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input: {
    padding: 4,
    height: 30,
    margin: 2,
    alignSelf: 'center',
    flex: 1
  },
  label: {
    fontSize: 14
  }
});

var Item = React.createClass({
  getInitialState: function(){
    return {
      textDefinision: "",
      textTerm: ""
    }
  },
  changeTextDefinision: function(text) {

    console.log(text);
    this.setState({
      textDefinision: text,
    })
    this.props.changeInput({
      textDefinision: text,
      textTerm: this.state.textTerm
    });
  },
  changeTextTerm: function(text) {
    console.log("term", arguments)
    this.setState({
      textTerm: text,
    })
    this.props.changeInput({
      textDefinision: this.state.textDefinision,
      textTerm: text
    });
  },
  render: function(){

    return (
      <View style={styles.input_wrap_words}>
        <TextInput
          onChangeText={this.changeTextTerm}
          style={styles.input}
          autoCapitalize={'none'}
          placeholder={'Term'}
          value={this.state.textTerm}
          />
        <TextInput
          onChangeText={this.changeTextDefinision}
          value={this.state.textDefinision}
          style={styles.input}
          autoCapitalize={'none'}
          placeholder={'Definision'}
          />
      </View>
    )
  }
})

module.exports = React.createClass({
  getInitialState: function() {
    return {
      title: '',
      countInputs: 1,
      inputs: [
          {
            id: 1
          }
      ]
    }
  },
  addElement: function(){
    var oldState = this.state.inputs.slice()
    oldState.push({
      id: oldState[oldState.length - 1].id + 1
    });
    this.setState({
      inputs: oldState
    })

  },
  changeInput: function(index, value){
    console.log("2222", index, value);
  },
  render: function () {
    var _this = this;
    var inputs = this.state.inputs.map(function(item, index){
      return <Item key={index} data={item} changeInput={_this.changeInput.bind(_this, index)} />
    });

    return (
      <View style={styles.mainContainer}>
        <View style={styles.toolbar}>
          <Text style={styles.toolbarButton}></Text>
          <Text style={styles.toolbarTitle}>Create study set</Text>
          <Text style={styles.toolbarButton}>Done</Text>
        </View>
        <View style={styles.content}>
          <View style={styles.input_wrap}>
            <TextInput
              style={styles.input}
              value={this.state.title}
              onChangeText={(text) => this.setState({title: text})}
              autoCapitalize={'none'}
              placeholder={'Title'}
              />
          </View>
          {inputs}
        </View>
        <View style={styles.footer}>
          <TouchableHighlight
            underlayColor={'#81c04d'}
            onPress={this.addElement}
            >
            <Text style={styles.plusButton}>+</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});
