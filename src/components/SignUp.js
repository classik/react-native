import React from 'react';

import {
  Text,
  View,
  TextInput,
  StyleSheet
} from 'react-native';

var Button = require('./common/Button');
var validator = require('email-validator');
var api = require('../utils/api');
var userStorage = require('../storage/user');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'red'
  },
  input : {
    padding: 4,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    margin: 5,
    width: 200,
    alignSelf: 'center'
  },
  label: {
    fontSize: 18
  }
});

module.exports = React.createClass({
  getInitialState: function() {
    return {
      email: '',
      password: '',
      confirm_password: '',
      errorMessage: ''
    }
  },
  onPressSignUp: function() {
    var email = this.state.email,
        password = this.state.password,
        confirm_password = this.state.confirm_password;
    if (!validator.validate(email)) {
      this.setState({errorMessage: 'Email not valid'});
    } else if (password === '' || confirm_password === '' || password !== confirm_password) {
      this.setState({errorMessage: 'Password doesn\'t match'});
    } else {
      this.setState({errorMessage: ''});
      api.registerUser(email, '', '', 0, '', '', password)
        .then((data) => {
          userStorage.write(data);
          api.getStudyList(data.response.data.token).then((response) => {
              this.props.navigator.immediatelyResetRouteStack([{name: 'study_list', data: response}]);
          });
        });
    }
  },
  onPressSignIn: function() {
    this.props.navigator.immediatelyResetRouteStack([{name: 'signin'}]);
  },
  render() {
    var showErr = (
      <Text style={styles.label}>{this.state.errorMessage}</Text>
    );
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Email:</Text>
        <TextInput
          style={styles.input}
          value={this.state.email}
          onChangeText={(text) => this.setState({email: text})}
          autoCapitalize={'none'}
          placeholder={'You email'}
          />

        <Text style={styles.label}>Password:</Text>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          value={this.state.password}
          onChangeText={(text) => this.setState({password: text})}
          autoCapitalize={'none'}
          placeholder={'Password'}
          />

        <Text style={styles.label}>Confirm password:</Text>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          value={this.state.confirm_password}
          onChangeText={(text) => this.setState({confirm_password: text})}
          autoCapitalize={'none'}
          placeholder={'Confirm password'}
          />

        {showErr}
        <Button text={'Sign up'} onPress={this.onPressSignUp} />
        <Button text={'Sign in'} onPress={this.onPressSignIn} />
      </View>
    );
  }
});
