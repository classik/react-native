import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  ListView,
  TextInput,
  AsyncStorage,
} from 'react-native';

var userStorage = require('../storage/user');
var studySetStorage = require('../storage/study_set');
var CreatSetButton = require('./common/Button');
var Item = require('./common/InputItem');
var api = require('../utils/api');

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  content: {
    flex: 1,
  },
  toolbar:{
        backgroundColor:'#81c04d',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row'
  },
  toolbarButton:{
      width: 50,
      color:'#fff',
      textAlign:'center'
  },
  toolbarTitle:{
      color:'#fff',
      textAlign:'center',
      fontWeight:'bold',
      flex:1
  },
  plusButton: {
    color:'#fff',
    textAlign:'right',
    fontWeight:'bold',
    fontSize: 22
  },
  footer: {
    position: 'absolute',
    left:0,
    right:0,
    bottom:0,
    backgroundColor:'#81c04d',
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight:10
  },
  input_wrap: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  input: {
    padding: 4,
    height: 30,
    margin: 2,
    alignSelf: 'center',
    flex: 1
  },
  label: {
    fontSize: 14
  },
  errWrap: {
    borderBottomColor: 'red',
    borderBottomWidth: 1,
    flexDirection:'row',
    margin: 5
  },
  errText: {
      fontSize: 14,
      color: 'red'
  }
});

module.exports = React.createClass({
  getInitialState: function() {
    var defaultInputVal = [];
    defaultInputVal[0] = {textDefinision:"", textTerm : ""}
    return {
      title: '',
      count_inputs: 0,
      input_values: defaultInputVal,
      errMsg: '',
    }
  },
  addElement: function(){
    var countInputs = this.state.count_inputs + 1;
    this.setState({
      count_inputs: countInputs
    });
    var itemValues = this.state.input_values.slice();
    itemValues[countInputs] = {textDefinision:"", textTerm : ""};
    this.setState({
        input_values: itemValues
    });
  },
  changeInput: function(index, value){
    var itemValues = this.state.input_values.slice();
    itemValues[index] = value;
    this.setState({
        input_values: itemValues
    });
  },
  saveStudySet: function() {
    var errMsg = '',
        inputValues = this.state.input_values,
        countInputs = this.state.count_inputs,
        titleValue = this.state.title,
        storageKey = this.state.storageKey;
    if (titleValue === '') {
      errMsg = 'Title can\'t be empty';
    } else if (inputValues.length != 0) {
      for (var i = 0; i <= countInputs; i++) {
        if (inputValues[i].textTerm == '' || inputValues[i].textDefinision == '') {
            errMsg = 'Fill all fields';
            break;
        }
      }
    }

    if (errMsg === '') {
      var words = inputValues.map(function(inputVal){
        var key = inputVal.textTerm;
        var val = inputVal.textDefinision;
        var word = new Object();
        word[key] = val;
        return word;
      });
      try {
        AsyncStorage.getItem('user').then((value) => {
          var userData = JSON.parse(value);
          api.saveStudySet(userData.token, titleValue, words, 'en').then((response) => {
              this.props.navigator.immediatelyResetRouteStack([{name: 'study_list', data:response, message:'New study set was added'}]);
          });
        }).done();
      } catch(error) {
        console.log('Error getting user data');
      }
    }


    this.setState({
      errMsg: errMsg
    });
  },
  onPressBackBtn: function() {
    this.props.navigator.immediatelyResetRouteStack([{name: 'study_list', data:[], message:''}]);
  },
  renderErrMsg: function() {
    if (this.state.errMsg !== '') {
      return (
        <View style={styles.errWrap}>
          <Text style={styles.errText}>{this.state.errMsg}</Text>
        </View>
      );
    }
  },
  render: function () {
    var _this = this;

    var item = function(index) {
        return <Item key={index} changeInput={_this.changeInput.bind(_this, index)} />
    }

    var inputs = [];
    for (var i = 0; i <= this.state.count_inputs; i++) {
      inputs.push(item(i));
    }

    return (
      <View style={styles.mainContainer}>
        <View style={styles.toolbar}>
          <TouchableHighlight
            underlayColor={'#81c04d'}
            onPress={this.onPressBackBtn}
            >
          <Text style={styles.toolbarButton}>Back</Text>
          </TouchableHighlight>
          <Text style={styles.toolbarTitle}>Create study set</Text>
          <TouchableHighlight
            underlayColor={'#81c04d'}
            onPress={this.saveStudySet}
            >
            <Text style={styles.toolbarButton}>Done</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.content}>
          {this.renderErrMsg()}
          <View style={styles.input_wrap}>
            <TextInput
              style={styles.input}
              value={this.state.title}
              onChangeText={(text) => this.setState({title: text})}
              autoCapitalize={'none'}
              placeholder={'Title'}
              />
          </View>
          {inputs}
        </View>
        <View style={styles.footer}>
          <TouchableHighlight
            underlayColor={'#81c04d'}
            onPress={this.addElement}
            >
            <Text style={styles.plusButton}>+</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});
